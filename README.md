# Symfony 2.5.0 for Benchmarking

## Installation

~~~
$ mkdir ~/tmp
$ cd ~/tmp
$ git clone git@bitbucket.org:kenjis/benchmark-symfony-2.5.0.git
$ cd benchmark-symfony-2.5.0
$ composer install
~~~

> Note: Just press Enter key to all questions.

~~~
$ chmod o+w app/cache/ app/logs/
~~~

~~~
$ cd /path/to/htdocs
$ ln -s ~/tmp/benchmark-symfony-2.5.0/web/ symf
~~~

## Benchmarking

~~~
$ siege -b -c 10 -t 3S http://localhost/symf/hello?name=BEAR
~~~
