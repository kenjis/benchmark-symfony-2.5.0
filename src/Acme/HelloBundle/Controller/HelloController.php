<?php

namespace Acme\HelloBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HelloController
{
    public function indexAction(Request $request)
    {
        $name = $request->query->get('name', 'World');
        return new Response('Hello ' . $name . '!');
    }
}
